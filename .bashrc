# .bashrc



# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias sudo='sudo '
alias ls='ls --color=auto'
alias xi='xbps-install'
alias xq='xbps-query'
alias xr='xbps-remove'

# PS1 Setup
PROMPT_COMMAND=__prompt_command

__prompt_command() {

    local HOSTCOLOR="5"
    local USERCOLOR="3"
    local PATHCOLOR="4"

    PS1="\e[3${HOSTCOLOR}m \h  \e[3${USERCOLOR}m \u  \e[3${PATHCOLOR}m \w  \n";
}
export PATH=$PATH:/var/lib/flatpak/exports/share
export PATH=$PATH:/home/alice/.local/share/flatpak/exports/share
export XDG_DATA_DIRS=$XDG_DATA_DIRS:/var/lib/flatpak/exports/share
export XDG_DATA_DIRS=$XDG_DATA_DIRS:/home/alice/.local/share/flatpak/exports/share
