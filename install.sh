#!/bin/sh
xbps-install -Su -y
xbps-install -Sv betterlockscreen -y
xbps-install -Sv vim -y
xbps-install -Sv xorg -y
xbps-install -Sv i3-gaps -y
xbps-install -Sv kitty -y
xbps-install -Sv font-awesome5 -y
xbps-install -Sv qutebrowser -y
xbps-install -Sv alsa-utils -y
xbps-install -Sv rofi -y
xbps-install -Sv alsa-plugins-ffmpeg -y
xbps-install -Sv maim -y
xbps-install -Sv polkit -y
xbps-install -Sv vifm -y
xbps-install -Sv feh -y
xbps-install -Sv gvim -y
xbps-install -Sv python3-dbus -y
xbps-install -Sv picom -y
xbps-install -Sv p7zip -y
xbps-install -Sv mpv -y
xbps-install -Sv youtube-dl -y
xbps-install -Sv lxappearance -y
xbps-install -Sv neofetch -y
xbps-install -Sv zsh -y
xbps-install -Sv dunst -y
xbps-install -Sv python3-pip -y
xbps-install -Sv tint2 -y
xbps-install -Sv curl -y
xbps-install -Sv zsh -y
xbps-install -Sv void-repo-nonfree -y
xbps-install -Sv arc-theme -y
xbps-install -Sv ntp -y
xbps-install -Sv xdg-desktop-portal
xbps-install -Sv xdg-desktop-portal-gtk
xbps-install -Sv xdg-user-dirs
xbps-install -Sv xdg-user-dirs-gtk
xbps-install -Sv xdg-utils
pip3 install pywal



sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
#pulseaudio and setup
xbps-install -Svy alsa-utils pulseaudio
ln -s /etc/sv/alsa /var/service/
ln -s /etc/sv/dbus /var/service/
ln -s /etc/sv/cgmanager /var/service/
ln -sf /usr/share/zoneinfo/America/Toronto /etc/localtime
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel10k
ntpdate 1.ro.pool.ntp.org
mkdir -p /home/$(logname)/Pictures/Screenshots/
cp -r /home/$(logname)/voidi3gaps/. /home/$(logname)/
rm -rf /home/$(logname)/voidi3gaps/
rm -rf /home/$(logname)/README.md
rm -rf /home/$(logname)/.git
#those line need to be last
chown -R $(logname) /home/$(logname)/
chgrp -R $(logname) /home/$(logname)/
chmod -R a+rwX,o-w /home/$(logname)
rm -rf /home/$(logname)/install.sh
