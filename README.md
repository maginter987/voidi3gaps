#!/bin/sh
xbps-install vim -y
xbps-install xorg -y
xbps-install i3-gaps -y
xbps-install kitty -y
xbps-install i3status -y
xbps-install py3status -y
xbps-install font-awesome5 -y
xbps-install firefox -y
xbps-install alsa-utils -y
xbps-install rofi -y
xbps-install alsa-plugins-ffmpeg -y
xbps-install maim -y
xbps-install i3lock -y
xbps-install polkit -y
xbps-install vifm -y
